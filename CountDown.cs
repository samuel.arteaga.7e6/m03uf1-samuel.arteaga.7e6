﻿using System;

namespace Ex2CountDown
{
    class CountDown
    {
        static void Main(string[] args)
        {
            int limite;
            limite = Convert.ToInt32(Console.ReadLine());
            for(int a=limite; a > 0 ;a--)
            {
                Console.Write(a);
            }
        }
    }
}
