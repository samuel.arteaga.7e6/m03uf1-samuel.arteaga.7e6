﻿using System;

namespace LetsCount
{
    class LetsCount
    {
        static void Main(string[] args)
        {
            int num = 0;
            int limite = Convert.ToInt32(Console.ReadLine());

            while (num < limite)
            {
                num++;
                Console.Write("{0} ", num);
            }
        }
    }
}
