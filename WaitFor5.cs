﻿using System;

namespace WaitFor5
{
    class WaitFor5
    {
        static void Main(string[] args)
        {
            int num = 0;
            Console.WriteLine("Introduzco un numero entero");
            int salida = 1;
            salida = Convert.ToInt32(Console.ReadLine());

            while (salida < 6) 
            {
                Console.Write(salida);
                salida++;
            }
        

        }
    }
}
