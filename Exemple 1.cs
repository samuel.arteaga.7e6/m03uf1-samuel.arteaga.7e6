﻿using System;

namespace Exemple_1
{
    class Program
    {
        static void Main(string[] args)
        {
            int num = 0;
            int limite = Convert.ToInt32(Console.ReadLine());

            while (num < limite)
            {
                num++;
                Console.Write("{0} ", num);
            }
        }
    }
}
