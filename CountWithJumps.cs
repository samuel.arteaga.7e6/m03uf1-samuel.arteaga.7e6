﻿using System;

namespace Ex3CountWithJumps
{
    class CountWithJumps
    {
        static void Main(string[] args)
        {
            int inici;
            int salt;
            int final;
            salt = Convert.ToInt32(Console.ReadLine());
            final = Convert.ToInt32(Console.ReadLine());
            
            for(int inicio=1 ;inicio <= final; inicio+=salt)
            {
                Console.WriteLine(inicio+ ",");
            }
        }
    }
}
