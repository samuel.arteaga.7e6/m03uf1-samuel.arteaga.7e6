﻿using System;

namespace MultiplyTable
{
    class MultiplyTable
    {
        static void Main(string[] args)
        {
            int num = 0;
            int limite = 1;
            num = Convert.ToInt32(Console.ReadLine());

            while (limite < 10)
            {
                Console.WriteLine($"{num}*{limite}={num * limite}");
                limite++;

            }              

        }
    }
}
